package ru.hexspeak.android.githubprojects;

/*
 * Подгрузка проектов без Retrofit
 * Не используется, просто для примера
 */

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GithubFetchr {

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }

            int bytesRead;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public List<Project> fetchProjects() {

        List<Project> projects = new ArrayList<>();

        try {
            String url = Uri.parse("https://api.github.com/search/repositories")
                    .buildUpon()
                    .appendQueryParameter("q", "game")
                    .appendQueryParameter("sort", "updated")
                    .appendQueryParameter("order", "desc")
                    .appendQueryParameter("per_page", "5")
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.i("FETCH", jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseItems(projects, jsonBody);
        } catch (JSONException je) {
            Log.e("FETCH", je.toString());
        } catch (IOException ioe) {
            Log.e("FETCH", ioe.toString());
        }
        return projects;
    }

    private void parseItems(List<Project> projects, JSONObject jsonBody)
            throws IOException, JSONException {

        JSONArray projectsJsonArray = jsonBody.getJSONArray("items");

        for (int i = 0; i < projectsJsonArray.length(); i++) {
            JSONObject projectJsonObject = projectsJsonArray.getJSONObject(i);

            Project project = new Project();
            project.setTitle(projectJsonObject.getString("name"));
            project.setFork(projectJsonObject.getBoolean("fork"));
            projects.add(project);
        }

    }
}
