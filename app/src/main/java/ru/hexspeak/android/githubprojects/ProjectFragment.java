package ru.hexspeak.android.githubprojects;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectFragment extends Fragment {

    private static final String ARG_PROJECT_ID = "project_id";

    private Project mProject;
    @BindView(R.id.project_title) TextView mTitleField;
    @BindView(R.id.project_date) TextView mDateField;
    @BindView(R.id.project_forked) CheckBox mForkedCheckBox;
    @BindView(R.id.project_description) TextView mDescriptionField;
    @BindView(R.id.project_language) TextView mLanguageField;

    public static ProjectFragment newInstance(UUID projectId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PROJECT_ID, projectId);
        ProjectFragment fragment = new ProjectFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID projectId = (UUID) getArguments()
                .getSerializable(ARG_PROJECT_ID);
        mProject = ProjectsStorage.get(getActivity()).getProject(projectId);
    }

    @Override
    public void onPause() {
        super.onPause();
        ProjectsStorage.get(getActivity()).updateProject(mProject);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_project, container, false);
        ButterKnife.bind(this, v);

        mTitleField.setText(mProject.getTitle());
        mDateField.setText(getString(R.string.created_format,
                DateFormat.format("yyyy.MM.dd hh:mm", mProject.getCreationDate()).toString()));
        mDescriptionField.setText(getString(R.string.description_format,mProject.getDescription()));
        mLanguageField.setText(getString(R.string.language_format, mProject.getLanguage()));
        mForkedCheckBox.setChecked(mProject.isFork());

        return v;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getActivity().onBackPressed();
        }
    }
}
