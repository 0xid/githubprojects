package ru.hexspeak.android.githubprojects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ru.hexspeak.android.githubprojects.ProjectDbSchema.ProjectTable;

public class ProjectsStorage {
    private  static ProjectsStorage sProjectsStorage;

    private SQLiteDatabase mDatabase;

    public static ProjectsStorage get(Context context) {
        if (sProjectsStorage == null) {
            sProjectsStorage = new ProjectsStorage(context);
        }
        return sProjectsStorage;
    }

    private ProjectsStorage(Context context) {
        mDatabase = new ProjectBaseHelper(context.getApplicationContext()).getWritableDatabase();
    }

    public List<Project> getProjects() {
        List<Project> projects = new ArrayList<>();

        ProjectCursorWrapper cursor = queryProjects(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                projects.add(cursor.getProject());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return projects;
    }

    public Project getProject(UUID id) {
        ProjectCursorWrapper cursor = queryProjects(
                ProjectTable.Cols.UUID + " = ?",
                new String[] {id.toString()}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getProject();
        } finally {
            cursor.close();
        }

    }

    private static ContentValues getContentValues(Project project) {
        ContentValues values = new ContentValues();
        values.put(ProjectTable.Cols.UUID, project.getId().toString());
        values.put(ProjectTable.Cols.TITLE, project.getTitle());
        values.put(ProjectTable.Cols.DESCRIPTION, project.getDescription());
        values.put(ProjectTable.Cols.LANGUAGE, project.getLanguage());
        values.put(ProjectTable.Cols.DATE, project.getCreationDate().getTime());
        values.put(ProjectTable.Cols.FORKED, project.isFork() ? 1 : 0);
        return values;
    }

    private ProjectCursorWrapper queryProjects(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                ProjectTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new ProjectCursorWrapper(cursor);
    }

    public void addProject(Project p) {
        ContentValues values = getContentValues(p);
        mDatabase.insert(ProjectTable.NAME, null, values);
    }

    public void updateProject(Project project) {
        String uuidString = project.getId().toString();
        ContentValues values = getContentValues(project);

        mDatabase.update(ProjectTable.NAME, values,
                ProjectTable.Cols.UUID + " = ?",
                new String[] {uuidString});

    }

    public void deleteAllProjects() {
        mDatabase.delete(ProjectTable.NAME, null, null);
    }

    public void resetProjects(List<Project> projects) {
        deleteAllProjects();
        for (Project project: projects) {
            addProject(project);
        }
    }
}
