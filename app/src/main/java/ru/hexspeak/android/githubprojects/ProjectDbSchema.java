package ru.hexspeak.android.githubprojects;

public class ProjectDbSchema {
    public static final class ProjectTable {
        public static final String NAME = "projects";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DESCRIPTION = "description";
            public static final String LANGUAGE = "language";
            public static final String DATE = "date";
            public static final String FORKED = "forked";
        }
    }

}
