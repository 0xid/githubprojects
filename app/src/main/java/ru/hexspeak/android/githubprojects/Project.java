package ru.hexspeak.android.githubprojects;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

public class Project {

    private UUID mId;

    @SerializedName("name")
    private String mTitle;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("language")
    private String mLanguage;
    @SerializedName("created_at")
    private Date mCreationDate;
    @SerializedName("fork")
    private boolean mFork;

    public Project() {
        this(UUID.randomUUID());
    }

    public Project(UUID id) {
        mId = id;
        mCreationDate = new Date();
    }

    public UUID getId() { return  mId; }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public Date getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(Date creationDate) {
        mCreationDate = creationDate;
    }

    public boolean isFork() {
        return mFork;
    }

    public void setFork(boolean fork) {
        mFork = fork;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
