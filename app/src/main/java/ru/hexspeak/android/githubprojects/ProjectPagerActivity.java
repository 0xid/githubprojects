package ru.hexspeak.android.githubprojects;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProjectPagerActivity extends AppCompatActivity {

    private static final String EXTRA_PROJECT_ID =
            "ru.hexspeak.android.githubprojects.project_id";

    @BindView(R.id.activity_project_pager_view_pager) ViewPager mViewPager;
    private List<Project> mProjects;

    public static Intent newIntent(Context packageContext, UUID projectId) {
        Intent intent = new Intent(packageContext, ProjectPagerActivity.class);
        intent.putExtra(EXTRA_PROJECT_ID, projectId);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_pager);

        UUID projectId = (UUID) getIntent().getSerializableExtra(EXTRA_PROJECT_ID);

        ButterKnife.bind(this);
        mProjects = ProjectsStorage.get(this).getProjects();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Project project = mProjects.get(position);
                return ProjectFragment.newInstance(project.getId());
            }

            @Override
            public int getCount() {
                return  mProjects.size();
            }
        });

        for(int i = 0; i < mProjects.size(); i++) {
            if (mProjects.get(i).getId().equals(projectId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}

