package ru.hexspeak.android.githubprojects;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectsListFragment extends Fragment {

    @BindView(R.id.projects_recycler_view) RecyclerView mProjectsRecyclerView;
    private ProjectAdapter mAdapter;
    private Callbacks mCallbacks;

    public interface  Callbacks {
        void onProjectSelected(Project project);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_projects_list, container, false);
        ButterKnife.bind(this, view);
        mProjectsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mProjectsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        updateUI();
        return view;
    }

    public void updateUI() {
        ProjectsStorage projectsStorage = ProjectsStorage.get(getActivity());
        List<Project> projects = projectsStorage.getProjects();
        if (mAdapter == null) {
            mAdapter = new ProjectAdapter(projects);
            mProjectsRecyclerView.setAdapter(mAdapter);
        }
        else
            mAdapter.setProjects(projects);
            mAdapter.notifyDataSetChanged();

        updateSubtitle();

    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_project_list, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ((item.getItemId())) {
            case R.id.menu_item_fetch_projects:
                //new FetchProjectsTask(getActivity()).execute();
                GithubFetchrRetrofit.get().fetchProjects((GithubFetchrRetrofit.Callbacks)getActivity());
                return true;
            case R.id.menu_item_delete_all_projects:
                ProjectsStorage.get(getActivity()).deleteAllProjects();
                updateUI();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void updateSubtitle() {
        ProjectsStorage projectsStorage = ProjectsStorage.get(getActivity());
        int projectsCount = projectsStorage.getProjects().size();
        String subtitle = getString(R.string.subtitle_format, projectsCount);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setSubtitle(subtitle);

    }

    class ProjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Project mProject;
        @BindView(R.id.list_item_projects_title_text_view) TextView mTitleTextView;
        @BindView(R.id.list_item_projects_creation_date_text_view) TextView mCreationDateTextView;

        public ProjectHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void bindProject(Project project) {
            mProject = project;
            mTitleTextView.setText(mProject.getTitle());
            mCreationDateTextView.setText(DateFormat.format("yyyy.MM.dd hh:mm", mProject.getCreationDate()).toString());
        }

        @Override
        public void onClick(View v) {
            mCallbacks.onProjectSelected(mProject);
        }
    }

    private class ProjectAdapter extends RecyclerView.Adapter<ProjectHolder> {
        private List<Project> mProjects;

        public ProjectAdapter(List<Project> projects) {
            mProjects = projects;
        }

        @Override
        public ProjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_project, parent, false);
            return new ProjectHolder(view);
        }

        @Override
        public void onBindViewHolder(ProjectHolder holder, int position) {
            Project project = mProjects.get(position);
            holder.bindProject(project);
        }

        @Override
        public int getItemCount() {
            return mProjects.size();
        }

        public void setProjects(List<Project> projects) {
            mProjects = projects;
        }
    }

    /*
    Класс для ручного обновления проектов, не используется, оставлен для примера
    */
    public class FetchProjectsTask extends AsyncTask<Void, Void, List<Project>> {

        private Context mContext;

        public FetchProjectsTask(Context context) {
            mContext = context;
        }

        @Override
        protected List<Project> doInBackground(Void... params) {
            return new GithubFetchr().fetchProjects();
        }

        @Override
        protected void onPostExecute(List<Project> projects) {
            for (Project project : projects) {
                ProjectsStorage.get(mContext).addProject(project);
            }
            updateUI();
        }
    }
}
