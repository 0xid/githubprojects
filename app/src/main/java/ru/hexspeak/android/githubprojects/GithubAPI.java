package ru.hexspeak.android.githubprojects;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubAPI {

    @GET("search/repositories")
    Call<GithubResponseProjects> getProjects(@Query("q") String q,
                                             @Query("sort") String sort,
                                             @Query("order") String order,
                                             @Query("per_page") int perPage);
}
