package ru.hexspeak.android.githubprojects;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

public class ProjectsListActivity extends SingleFragmentActivity
implements  ProjectsListFragment.Callbacks, GithubFetchrRetrofit.Callbacks {

    private static final String BUNDLE_PROJECT_UUID = "projectUUID";

    private UUID selectedProjectUUID;

    @Override
    protected Fragment createFragment() {
        return new ProjectsListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    public void onProjectSelected(Project project) {
        selectedProjectUUID = project.getId();
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = ProjectPagerActivity.newIntent(this, project.getId());
            startActivity(intent);
        } else {
            Fragment newDetail = ProjectFragment.newInstance(project.getId());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, newDetail)
                    .commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_PROJECT_UUID)) {
            selectedProjectUUID = (UUID) savedInstanceState.getSerializable(BUNDLE_PROJECT_UUID);
            if ((findViewById(R.id.detail_fragment_container) != null)) {
                Project project = ProjectsStorage.get(this).getProject(selectedProjectUUID);
                if (project != null) {
                    onProjectSelected(project);
                }
                else {
                    selectedProjectUUID = null;
                }
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (selectedProjectUUID != null) {
            outState.putSerializable(BUNDLE_PROJECT_UUID, selectedProjectUUID);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onProjectsLoaded(List<Project> projects) {
        ProjectsStorage.get(this).resetProjects(projects);
        ProjectsListFragment listFragment = (ProjectsListFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_container);
        listFragment.updateUI();


        // Сброс контейнера с описанием фрагмента
        if (findViewById(R.id.detail_fragment_container) != null) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.detail_fragment_container);
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .detach(fragment)
                        .commit();
            }
        }

        Toast.makeText(this, "Projects updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProjectsLoadError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
