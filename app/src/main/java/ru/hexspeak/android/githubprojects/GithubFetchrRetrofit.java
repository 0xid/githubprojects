package ru.hexspeak.android.githubprojects;

import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GithubFetchrRetrofit {

    private static final String BASE_URL="https://api.github.com/";
    private static GithubFetchrRetrofit sGithubFetchrRetrofit;
    private GithubAPI mGithubAPI;

    private GithubFetchrRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mGithubAPI = retrofit.create(GithubAPI.class);
    }

    public static GithubFetchrRetrofit get() {
        if (sGithubFetchrRetrofit == null) {
            sGithubFetchrRetrofit = new GithubFetchrRetrofit();
        }
        return sGithubFetchrRetrofit;
    }


    public interface Callbacks {
        void onProjectsLoaded(List<Project> projects);
        void onProjectsLoadError(String error);
    }


    public void fetchProjects(final Callbacks callbacks) {
        Call<GithubResponseProjects> call = mGithubAPI.getProjects("game", "updated", "desc", 10);

        call.enqueue(new Callback<GithubResponseProjects>() {
            @Override
            public void onResponse(Call<GithubResponseProjects> call, Response<GithubResponseProjects> response) {
                if (response.isSuccessful()) {
                    GithubResponseProjects githubResponseProjects = response.body();
                    if (callbacks != null) {
                        callbacks.onProjectsLoaded(githubResponseProjects.items);
                    }
                }
                else {
                    if (callbacks != null) {
                        callbacks.onProjectsLoadError(response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<GithubResponseProjects> call, Throwable t) {
                if (callbacks != null) {
                    callbacks.onProjectsLoadError(t.getLocalizedMessage());
                }

            }
        });
    }

}
