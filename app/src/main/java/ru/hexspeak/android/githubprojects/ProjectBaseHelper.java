package ru.hexspeak.android.githubprojects;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.hexspeak.android.githubprojects.ProjectDbSchema.ProjectTable;

public class ProjectBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "projectBase.db";

    public ProjectBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + ProjectTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                ProjectTable.Cols.UUID + ", " +
                ProjectTable.Cols.TITLE + ", " +
                ProjectTable.Cols.DESCRIPTION + ", " +
                ProjectTable.Cols.LANGUAGE + ", " +
                ProjectTable.Cols.DATE + ", " +
                ProjectTable.Cols.FORKED +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
