package ru.hexspeak.android.githubprojects;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Date;
import java.util.UUID;

import ru.hexspeak.android.githubprojects.ProjectDbSchema.ProjectTable;


public class ProjectCursorWrapper extends CursorWrapper {
    public ProjectCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Project getProject() {
        String uuidString = getString(getColumnIndex(ProjectTable.Cols.UUID));
        String title = getString(getColumnIndex(ProjectTable.Cols.TITLE));
        String description = getString(getColumnIndex(ProjectTable.Cols.DESCRIPTION));
        String language = getString(getColumnIndex(ProjectTable.Cols.LANGUAGE));
        long date = getLong(getColumnIndex(ProjectTable.Cols.DATE));
        int isForked = getInt(getColumnIndex(ProjectTable.Cols.FORKED));

        Project project = new Project(UUID.fromString(uuidString));
        project.setTitle(title);
        project.setDescription(description);
        project.setLanguage(language);
        project.setCreationDate(new Date(date));
        project.setFork(isForked != 0);

        return project;
    }
}
